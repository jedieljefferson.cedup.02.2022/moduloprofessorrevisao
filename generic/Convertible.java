package com.example.primeiroprojetospring.generic;

public interface Convertible<T> {
    T convert();
}
