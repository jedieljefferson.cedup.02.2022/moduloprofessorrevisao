package com.example.primeiroprojetospring.dto;

import com.example.primeiroprojetospring.entity.CidadeEntity;
import com.example.primeiroprojetospring.generic.Convertible;

import java.util.Objects;

public class CidadeDTO implements Convertible<CidadeEntity> {
    private Integer id;
    private String nome;
    private EstadoDTO estadoDTO;

    public CidadeDTO() {
    }
    public CidadeDTO(Integer id, String nome, EstadoDTO estadoDTO) {
        this.id = id;
        this.nome = nome;
        this.estadoDTO = estadoDTO;
    }
    public CidadeDTO(CidadeEntity cidadeEntity) {
        this.id = cidadeEntity.getId();
        this.nome = cidadeEntity.getNome();
        this.estadoDTO = cidadeEntity.convert().getEstadoDTO();
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public EstadoDTO getEstadoDTO() {
        return estadoDTO;
    }
    public void setEstadoDTO(EstadoDTO estadoDTO) {
        this.estadoDTO = estadoDTO;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CidadeDTO cidadeDTO = (CidadeDTO) o;
        return Objects.equals(id, cidadeDTO.id);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
    @Override
    public CidadeEntity convert() {
        return new CidadeEntity(this);
    }
}
