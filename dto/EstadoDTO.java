package com.example.primeiroprojetospring.dto;

import com.example.primeiroprojetospring.entity.EstadoEntity;

import java.util.List;
import java.util.Objects;

public class EstadoDTO {
    private Integer id;
    private String nome;
    private List<CidadeDTO> cidadeDTOS;

    public EstadoDTO() {
    }
    public EstadoDTO(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }
    public EstadoDTO(EstadoEntity estadoEntity) {
        this.id = estadoEntity.getId();
        this.nome = estadoEntity.getNome();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<CidadeDTO> getCidadeDTOS() {
        return cidadeDTOS;
    }

    public void setCidadeDTOS(List<CidadeDTO> cidadeDTOS) {
        this.cidadeDTOS = cidadeDTOS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EstadoDTO estadoDTO = (EstadoDTO) o;
        return Objects.equals(id, estadoDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
