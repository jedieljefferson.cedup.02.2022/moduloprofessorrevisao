package com.example.primeiroprojetospring.entity;

import com.example.primeiroprojetospring.dto.CidadeDTO;
import com.example.primeiroprojetospring.generic.Convertible;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "cidades")
public class CidadeEntity implements Serializable, Convertible<CidadeDTO> {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;
    @ManyToOne
    @JoinColumn(name = "estado_id")
    private EstadoEntity estadoEntity;

    public CidadeEntity() {
    }
    public CidadeEntity(Integer id, String nome, EstadoEntity estadoEntity) {
        this.id = id;
        this.nome = nome;
        this.estadoEntity = estadoEntity;
    }
    public CidadeEntity(CidadeDTO cidadeDTO) {
        this.id = cidadeDTO.getId();
        this.nome = cidadeDTO.getNome();
        this.estadoEntity = cidadeDTO.convert().getEstadoEntity();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public EstadoEntity getEstadoEntity() {
        return estadoEntity;
    }

    public void setEstadoEntity(EstadoEntity estadoEntity) {
        this.estadoEntity = estadoEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CidadeEntity that = (CidadeEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public CidadeDTO convert() {
        return new CidadeDTO(this);
    }
}
