package com.example.primeiroprojetospring.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "estados")
public class EstadoEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;

    @OneToMany(mappedBy="estadoEntity")
    private List<CidadeEntity> cidadeEntities;

    public EstadoEntity() {
    }
    public EstadoEntity(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }
    public EstadoEntity(EstadoEntity estadoEntity) {
        this.id = estadoEntity.getId();
        this.nome = estadoEntity.getNome();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<CidadeEntity> getCidadeEntities() {
        return cidadeEntities;
    }

    public void setCidadeEntities(List<CidadeEntity> cidadeEntities) {
        this.cidadeEntities = cidadeEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EstadoEntity that = (EstadoEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
